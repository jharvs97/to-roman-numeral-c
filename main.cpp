#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "string_builder.cpp"

#define ArrayLength(A) sizeof(A)/sizeof(A[0])

char *ToRoman(uint64_t Number)
{
    string_builder Builder = StringBuilderCreate();
    
    int NumeralValues[] = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
    const char *NumeralChars[] = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
    
    static_assert(ArrayLength(NumeralValues) == ArrayLength(NumeralChars));
    
    size_t NumeralCount = ArrayLength(NumeralValues);
    
    uint64_t Current = Number;
    
    for (size_t NumeralIndex = 0;
         NumeralIndex < NumeralCount;
         NumeralIndex++)
    {
        int Quotient  = Current / NumeralValues[NumeralIndex];
        Current %= NumeralValues[NumeralIndex];
        
        for (int J = 0;
             J < Quotient;
             J++)
        {
            StringBuilderAppend(&Builder, NumeralChars[NumeralIndex]);
        }
    }
    
    return StringBuilderToOwnedCString(&Builder);
}

int main(int ArgCount, char **Args)
{
    for (int I = 1;
         I < ArgCount;
         I++)
    {
        printf("%d: %s\n", I, ToRoman(atoi(Args[I])));
    }
}