struct string_builder
{
    char *Buffer;
    size_t BufferCapacity;
    size_t WriteIndex;
};

static constexpr size_t BufferCapacityIncrement = 2;

string_builder
StringBuilderCreate()
{
    string_builder Builder = {};
    Builder.BufferCapacity = BufferCapacityIncrement;
    Builder.Buffer = (char *) malloc(Builder.BufferCapacity);
    
    return Builder;
}

void
StringBuilderDestroy(string_builder *Builder)
{
    if (Builder != 0)
    {
        free(Builder->Buffer);
        *Builder = {};
    }
}

void
StringBuilderCheckForCapacityAndGrow(string_builder *Builder, size_t ToAppendLength)
{
    size_t CapacityAfterWrite = Builder->WriteIndex + ToAppendLength;
    if (CapacityAfterWrite > Builder->BufferCapacity)
    {
        size_t ExtraCapacity = (((CapacityAfterWrite - Builder->BufferCapacity) / BufferCapacityIncrement) + 1) * BufferCapacityIncrement;
        
        char *PrevBuffer = Builder->Buffer;
        size_t PrevCapacity = Builder->BufferCapacity;
        
        Builder->BufferCapacity += ExtraCapacity;
        Builder->Buffer = (char *) malloc(Builder->BufferCapacity);
        memcpy(Builder->Buffer, PrevBuffer, PrevCapacity);
        free(PrevBuffer);
    }
}

void
StringBuilderAppend(string_builder *Builder, const char *ToAppend)
{
    size_t ToAppendLength = strlen(ToAppend);
    StringBuilderCheckForCapacityAndGrow(Builder, ToAppendLength);
    
    memcpy(Builder->Buffer + Builder->WriteIndex, ToAppend, ToAppendLength);
    Builder->WriteIndex += ToAppendLength;
}

// Creates a null-terminated string from the builder and destroys the string_builder
char *
StringBuilderToOwnedCString(string_builder *Builder)
{
    char *CString = (char *)malloc(Builder->WriteIndex + 1);
    memcpy(CString, Builder->Buffer, Builder->WriteIndex);
    CString[Builder->WriteIndex] = 0;
    
    StringBuilderDestroy(Builder);
    
    return CString;
}
